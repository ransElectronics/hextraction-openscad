eps=1/128;
$fn=48;

ruleCard(renderHex=true);

module ruleCard(renderHex=false) {
    difference() {
        union() {
            difference() {
                minkowski() {
                    translate([0,0,-1.4])
                        linear_extrude(0.8,center=true)
                            offset(r=-0.6)
                                doubleOffset(4) {
                                    unitHex();
                                    translate([96.7515/2,0])
                                        square([96.7515,50],center=true);
                                }
                    sphere(d=1.2);
                }
                translate([90.753,19,-1.4])
                    linear_extrude(30,center=true)
                        square(20);
            }
            translate([90.753,19,-1.4])
                intersection() {
                    minkowski() {
                        linear_extrude(0.8,center=true)
                            circle(d=12-1.2);
                        sphere(d=1.2);
                    }
                    linear_extrude(30,center=true)
                        square(20);
                }
        }
        translate([90.753,19,-1.4])
            rotate_extrude(angle=360)
                difference() {
                    translate([0,-5])
                        square([5,10]);
                    doubleOffset(0.6)
                        translate([3,-2/2-eps])
                            square([10,2+eps+eps]);
                } 
    }
    hull() {
        translate([0,0,-2])
        linear_extrude(4.0) 
            doubleOffset(4)
                unitHex();
        linear_extrude(2.6) 
            offset(-0.6)
            doubleOffset(4)
                unitHex();
    }
}
module unitHex() {
    circle(d=50*sqrt(3)/1.5,$fn=6);
}

module star(r) {
    scale([r,r,1])
    polygon([
        [0, 1], [-0.224514, 0.309017], 
        [-0.951057, 0.309017], [-0.363271, -0.118034], 
        [-0.587785, -0.809017], [0, -0.381966], 
        [0.587785, -0.809017], [0.363271, -0.118034], 
        [0.951057, 0.309017], [0.224514, 0.309017]
    ]);
}

module sphere(r = 1, d = undef) { 
    R = is_undef(d) ? r : d / 2;
    rotate_extrude()
        intersection() {
            circle(r=R);
            translate([0,-R])
                square([R,R*2]);
        }
}
 
module doubleOffset(R) {
    offset(r=R)
        offset(r=-R)
            children();
}