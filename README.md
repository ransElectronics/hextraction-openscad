This is an Openscad recreation of the board game Hextraction made by Zack Friedman.

This repo also contains the CAD files for all of the custom tiles that I have made.


== Files ==

* rule_card.scad

Contains a blank template for a rule card

* plate.scad

Generates a hextraction grid based on parameters that can be easily entered in OpenSCAD's customizer

* basic.scad

Lets you create any basic tile you want. Using just the built in customizer, it is possible to make every tile in the original basic tile set.  The fun part is that it can make combinations that you have never seen before! It also lets you create mystery tiles that are compatible with Zack Friedman's mystery tile lids. 

* mystery_lid.scad
 
 OpenSCAD recreation of the lid for Mystery tiles. 

* binary/

A binary flip-flop tile I created. STLs can be downloaded here: https://thangs.com/designer/ransElectronics/3d-model/Binary%20Tile%20%28hextraction%29-884718

* vase_mode/

A basic Hextraction tile that prints in vase mode.  STLs can be downloaded here: https://thangs.com/designer/ransElectronics/3d-model/%5BHextraction%5D%20Vase%20Mode%20Basic%20Tile-902660

* strip/ 

Drop-in replacements for the Mystery tile lids. They add Forbidden triggers to either remove or put on clothing. https://thangs.com/designer/ransElectronics/3d-model/%5BHextraction%5D%20Dress%20and%20Strip%20Mystery%20Tiles-899753