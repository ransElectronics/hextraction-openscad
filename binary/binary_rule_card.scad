eps=1/128;
$fn=48;
 use <../rule_card.scad>
rules("binary tile",0);
rules("effect:",1);
rules("tile does not get",2);
rules("removed if a ball",3);
rules("exits under the",4);
rules("flip-flop.",5);
 
*color("darkGreen")
     translate([48.335,1.1,0])
        cylinder(d=0.8,h=4);
difference() {
    union()
        ruleCard(renderHex=true);
    translate([0,0,10]) {
        cube([11,80,20],center=true);
        cube([14,30,20],center=true);
    }
    translate([-50*sqrt(3)/3,0,0.4+eps])
        difference() {
            cylinder(h=5,d=50*(2/3)+8);
            cylinder(h=11,d=50*(2/3)-8,center=true);
        }
    translate([50*sqrt(3)/3,0,0.4+eps])
        difference() {
            cylinder(h=5,d=50*(2/3)+8);
            cylinder(h=11,d=50*(2/3)-8,center=true);
        }
    translate([0,12,0])
        cylinder(h=20,d=10,center=true);
    cube([10,5,10],center=true);
}
intersection() {
    union() {
        translate([0,3.5,-1])
            rotate([22.5,0,0])
                linear_extrude(3,center=true)
                    square([10,11],center=true);
        translate([0,-3.5,-1])
            rotate([-22.5,0,0])
                linear_extrude(3,center=true)
                    square([10,11],center=true);
    }
    cube([15,20,4.8],center=true);
}

module rules(string,row) {
color("darkBlue")
    translate([30,18-row*6,-0.4])
        linear_extrude(0.8)
              text(string, 
                   size = 5,
                   spacing=1,
                   font = "Makisupa:style=Regular",
                   $fn = 16);
}