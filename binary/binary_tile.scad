$fn=24;
show_tile=true;
show_flipper=false;
flipAngle=0;//[0:1:80]
eps=1/128;
use <../basic.scad>
use <../plate.scad>

//rotate([0,90,0]) toggle();

//3d stuff
if (show_flipper) {
    rotate([-30,0,0])
        translate([0,-2.5,3])
            rotate([flipAngle,0,0])
                toggle();
}
//color(alpha=0.3)
if (show_tile)
    rotate([-30,0,0])
        tile();
*color("#333333")
    rotate([30,0,0])
        translate([0,0,-3-eps])
            plate();

 /* //2d stuff
 *translate([14,6])
 circle(d=10);
translate([3,-2.5])
    rotate([0,0,-58])
        toggle();

color("darkBlue")
    linear_extrude(eps*2)
        projection(cut=true)
            rotate([0,90,0])
                tile();
#linear_extrude(eps)
    projection(cut=false)
        rotate([0,90,0])
            tile();
*/

module toggle() {
    rotate([0,-90,0]) {
        difference() {
            union() {
                linear_extrude(13,center=true,convexity=3)
                    toggleTrough();
                translate([0,0,6])
                    linear_extrude(2,center=false,convexity=3)
                        toggleWall();
                translate([0,0,-8])
                    linear_extrude(2,center=false,convexity=3)
                        toggleWall();
            }
           linear_extrude(height=16+eps,center=true,convexity=1,twist=540,slices=20)
                translate([0.5,0,0])
                    circle(d=2.5);
            translate([0.3,-0.3,-8.1]) //hole for jamming the filament in
                cylinder(d1=4,d2=2.3,h=2,center=false);
        }
    }
}

module toggleTrough() {
    union() {
        hull() { //The reset lever
            translate([-4,7])
                circle(d=2);
            translate([0,0])
                circle(d=2);
        }
        hull() {
            translate([0,0]) 
                circle(d=5);
            translate([12,12.5]) 
                circle(d=1);
        }
        hull() {
            translate([0,0]) 
                circle(d=5);
             translate([9,-8]) 
                 circle(d=1);
        }
    }
}

module toggleWall() {
        union() {
            hull() {
                translate([12,12.5]) 
                    circle(d=1);
                translate([0,0]) 
                    circle(d=5);
                translate([9,-8]) 
                    circle(d=1);
                *translate([18,-8]) 
                    circle(d=1);
                translate([18,12.5]) 
                    circle(d=1);
            }
            hull() { //The reset lever
                translate([-4,7])
                    circle(d=2);
                translate([0,0])
                    circle(d=2);
            }
        }
}

module tile() {
    difference() {
        rotate([30,0,0])
            translate([0,6,0])
            difference() {
                basic_tile();
                /* translate([11,19,13])
                    linear_extrude(50,convexity=4)
                        star(4); */
                rotate([0,0,180])
                    a_cut_half();
                rotate([0,0,120])
                    c_cut($fn=48);
                rotate([0,0,300])
                    c_cut($fn=48);
                rotate([0,0,0])
                    notch_cutout();
                rotate([0,0,60])
                    notch_cutout();
                rotate([0,0,300])
                    notch_cutout();
            }
        translate([0,0,5.65])
            rotate([20,0,0])
                a_cut();
        translate([0,-3,0])
            linear_extrude(50)
                square([17,27],center=true);
        translate([0,-8,0])
            linear_extrude(50)
                square([11,27],center=true);
        translate([0,13,0])
            rotate([30,0,0])
                linear_extrude(60)
                    circle(d=11);
        translate([0,-2.5,3])
            rotate([0,90,0]) {
                translate([0,0,-10])
                    cylinder(d=2.2,h=20,center=false);
                rotate([0,0,0])
                    translate([0,0,-16])
                        cylinder(d=2.0,h=5,center=false);
                translate([0,0,-11.8])
                    cylinder(d1=2, d2=5,h=3.4);
                translate([0,0,9.8])
                    rotate([0,-3.7,30]) {
                        cylinder(d=2.2,h=8);
                        translate([0,0,8-eps])
                            cylinder(d1=2.2, d2=2.6,h=10);
                        translate([-0.6,0,10-eps])
                            rotate([0,-3,0])
                                cylinder(d1=1.1,d2=2.2,h=10,$fn=4);
                        }
            }
    }
    rotate([30,0,0])
        translate([0,-13.4,14.5])
            rotate([-45,0,0])
                multmatrix(m = [ [1, 0,    0, 0],
                                 [0, 1,    0, 0],
                                 [0, sin(45), 1, 0],
                                 [0, 0,    0,  1]
                          ])
                        difference() {
                             linear_extrude(15)
                                square([23,4],center=true);
                             rotate([90,0,0]) {
                                linear_extrude(8,center=true)
                                    intersection() {
                                        rotate([0,0,45])
                                            square(11,center=true);
                                        rotate([0,0,0])
                                            square([11,16],center=true);
                                    }
                                }
                        }
}
