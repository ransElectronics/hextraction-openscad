reproduction=false;
module __CUSTOMIZER_LIMIT() {}
$fn=6; //Reappropriate circles as hexagons lol
hexSize=51;
unitHex=sqrt(3)*2/3;
unitY=hexSize;
unitX=unitHex*hexSize;
eps=1/128;
spacing=0.25;

gutter_segment();

module gutter_segment() {
    difference() {
        intersection() {
            contour_outer();
            union() {
                contour_bottom();
                segment_top();
            }
        }
        bottom_decor_half_3d();
        mirror([1,0,0])
            bottom_decor_half_3d();
    }
}

module bottom_decor_half_3d() {
translate([0.8,0,-21])
    multmatrix([[1.285,0,-sin(30),0],
                [0,1,0,0],
                [0,0,1,0]])
    linear_extrude(18-eps,convexity=5) {
        doubleOffset(r=1.3,$fn=48) {
        bottom_decor_quarter();
        mirror([0,1,0])
            bottom_decor_quarter();
        }
    }
}

module bottom_decor_quarter() {
    intersection() {
        difference() {
            union() {
                translate([11,16])
                    rotate([0,0,30])
                        circle(d=26.5,$fn=3);
                circle(d=19.6,$fn=6);
            }
        translate([0,13.75])
            square([70,3],center=true);
        square([10.1,100],center=true);
        }
        translate([-eps,-eps])
            square(50);
    }
}

module contour_bottom() {
if (reproduction) {
    translate([0,0,22.685])
        rotate([90,90,0])
            linear_extrude(unitY-spacing*2,center=true,convexity=5)
                difference() {
                doubleOffset(r=10,$fn=96)
                    circle(d=unitX*1.5,$fn=6);
                doubleOffset(r=10,$fn=96)
                    circle(d=unitX*1.5-6.93,$fn=6);
                }
} else {
    translate([0,0,22.685])
        rotate([90,90,0])
            linear_extrude(unitY-spacing*2,center=true,convexity=5)
                difference() {
                    rotate([0,0,45])
                        square(75,center=true);
                doubleOffset(r=10,$fn=96)
                    circle(d=unitX*1.5-6.93,$fn=6);
                }
}
}

module contour_outer() {
    intersection() {
        hull() 
            contour_bottom();
        translate([0,0,-5])
            linear_extrude(30,convexity=2,center=true) {
                offset(delta=-spacing)
                    difference() {
                        square([unitX,unitY],center=true);
                        union() {
                            translate([unitX*3/4,0])
                                circle(d=unitX);
                            translate([-unitX*3/4,0])
                                circle(d=unitX);
                        }
                    }
                }
    }
}

module segment_top() {
translate([0,0,-32])
    linear_extrude(30,convexity=3) {
        offset(delta=-spacing)
            difference() {
                square([unitX,unitY],center=true);
                union() {
                    doubleOffset(r=-15,$fn=96) {
                    translate([0,-unitY/2])
                        rotate([0,0,90])
                            circle(d=(unitX-12.366)*sqrt(3),$fn=3);
                    translate([0,unitY/2])
                        rotate([0,0,30])
                            circle(d=(unitX-12.366)*sqrt(3),$fn=3);
                    }
                    translate([unitX*3/4,0])
                        circle(d=unitX);
                    translate([-unitX*3/4,0])
                        circle(d=unitX);
                }
            }
            end_feature();
        }
translate([0,0,-3])
    linear_extrude(3) {
        offset(delta=-spacing)
            difference() {
                square([unitX,unitY],center=true);
                translate([unitX*3/4,0])
                    circle(d=unitX);
                translate([-unitX*3/4,0])
                    circle(d=unitX);
                difference() {
                    union() {
                        translate([0,unitY/2])
                            circle(d=unitX-12.365);
                        translate([0,-unitY/2])
                            circle(d=unitX-12.365);
                    }
                    square([50,19.833],center=true);
                }
            }
        end_feature();
    }
translate([0,unitY/2,0])
    linear_extrude(3) {
        offset(delta=-spacing) {
            rotate([0,0,120])
                sideTab();
            rotate([0,0,240])
                sideTab();
            translate([0,-unitY,0]) {
                rotate([0,0,0])
                    sideTab();
                rotate([0,0,60])
                    sideTab();
                rotate([0,0,300])
                    sideTab();
                polygon([
                    [5.85,23.125],[5.85,20.5],[7.85,18.5],
                    [-7.85,18.5],[-5.85,20.5],[-5.85,23.125]
                ]);
            }
            intersection() {
                union() {
                    circle(d=37*unitHex);
                    translate([0,-unitY,0])
                        circle(d=37*unitHex);
                }
                translate([0,-unitY/2,0])
                    square([50,19.833],center=true);
            }
        }
    }
}

module end_feature() {
    translate([(unitX/2-7.265),(unitY/2-2.415)])
        circle(d=5);
    translate([(unitX/2-7.265),(unitY/2-2.415)*-1])
        circle(d=5);
    translate([(unitX/2-7.265)*-1,(unitY/2-2.415)])
        circle(d=5);
    translate([(unitX/2-7.265)*-1,(unitY/2-2.415)*-1])
        circle(d=5);
}

module sideTab() {
   polygon([
        [5.85,23.125],[5.85,20.1464],
        [-5.85,20.1464],[-5.85,23.125]
   ]); 
}

module doubleOffset(r=0) {
    R=r;
    offset(r=R)
        offset(r=-R)
            children();
}
