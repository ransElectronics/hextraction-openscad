dress=false;

use <../basic.scad>
use <../rule_card.scad>
use <strip_mystery.scad>

if (dress) {
    dress_card();
} else {
    strip_card();
}

module dress_card() {
rules("    dress tile",0);
rules("forbidden trigger:",1.5);
rules("player puts on one",2.5);
rules("article of clothing.",3.5);
rules("players may vote",5);
rules("on placement limit",6);

    difference() {
        ruleCard();
        translate([0,0,1]) 
            linear_extrude(6,convexity=5) {
                dress_icon();
                #translate([10,18])
                     star(4);
            }
    }
}

module strip_card() {
rules("    strip tile",0);
rules("forbidden trigger:",1.5);
rules("player removes one",2.5);
rules("article of clothing.",3.5);
rules("players may vote",5);
rules("on placement limit",6);
    difference() {
        ruleCard();
        translate([0,0,1]) 
            linear_extrude(6,convexity=5) {
                strip_icon();
                translate([10,18])
                     star(4);
            }
    }
}
module rules(string,row) {
color("darkBlue")
    translate([29,17-row*6,-0.4])
        linear_extrude(0.8)
              text(string, 
                   size = 4.5,
                   spacing=1,
                   font = str("Makisupa:style=Regular"),
                   $fn = 16);
}