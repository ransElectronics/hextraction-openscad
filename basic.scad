/* [Secret?] */
secret_tile=false;
/* [Path Type A] */
A_path_1=false;
A_path_2=false;
A_path_3=false;
A_path_4=false;
A_path_5=false;
A_path_6=false;
/* [Path Type B] */
Make_B_path_straight=false;
B_path_1=false;
B_path_2=false;
B_path_3=false;
B_path_4=false;
B_path_5=false;
B_path_6=false;
/* [Path Type C] */
C_path_1=false;
C_path_2=false;
C_path_3=false;
C_path_4=false;
C_path_5=false;
C_path_6=false;
/* [Dead Ends] */
Dead_end_1=false;
Dead_end_2=false;
Dead_end_3=false;
Dead_end_4=false;
Dead_end_5=false;
Dead_end_6=false;
/* [Orientation notch positions] */
Key_notch_1=true;
Key_notch_2=true;
Key_notch_3=true;
Key_notch_4=true;
Key_notch_5=true;
Key_notch_6=true;

module __CUSTOMIZER_LIMIT() {}
$fn=48;
hexSize=50;
unitY=hexSize;
unitX=hexSize*sqrt(3)*2/3;
eps=1/128;

difference() {
    basic_tile();
    if (secret_tile) secret_top();
    if (A_path_1 ||A_path_2 ||A_path_3 ||
        A_path_4 ||A_path_5 ||A_path_6)
            middle_hole();
    if (A_path_1) rotate([0,0,0])    a_cut_half();
    if (A_path_2) rotate([0,0,60])   a_cut_half();
    if (A_path_3) rotate([0,0,120])  a_cut_half();
    if (A_path_4) rotate([0,0,180])  a_cut_half();
    if (A_path_5) rotate([0,0,240])  a_cut_half();
    if (A_path_6) rotate([0,0,300])  a_cut_half();
    if (B_path_1) rotate([0,0,0])    b_cut();
    if (B_path_2) rotate([0,0,60])   b_cut();
    if (B_path_3) rotate([0,0,120])  b_cut();
    if (B_path_4) rotate([0,0,180])  b_cut();
    if (B_path_5) rotate([0,0,240])  b_cut();
    if (B_path_6) rotate([0,0,300])  b_cut();
    if (C_path_1) rotate([0,0,0])    c_cut();
    if (C_path_2) rotate([0,0,60])   c_cut();
    if (C_path_3) rotate([0,0,120])  c_cut();
    if (C_path_4) rotate([0,0,180])  c_cut();
    if (C_path_5) rotate([0,0,240])  c_cut();
    if (C_path_6) rotate([0,0,300])  c_cut();
    if (Dead_end_1) rotate([0,0,0])   dead_end();
    if (Dead_end_2) rotate([0,0,60])  dead_end();
    if (Dead_end_3) rotate([0,0,120]) dead_end();
    if (Dead_end_4) rotate([0,0,180]) dead_end();
    if (Dead_end_5) rotate([0,0,240]) dead_end();
    if (Dead_end_6) rotate([0,0,300]) dead_end();
    if (Key_notch_1) rotate([0,0,0])   notch_cutout();
    if (Key_notch_2) rotate([0,0,60])  notch_cutout();
    if (Key_notch_3) rotate([0,0,120]) notch_cutout();
    if (Key_notch_4) rotate([0,0,180]) notch_cutout();
    if (Key_notch_5) rotate([0,0,240]) notch_cutout();
    if (Key_notch_6) rotate([0,0,300]) notch_cutout();
}

/*
   _______
  /   1   \
 /6       2\
/           \
\           /
 \5       3/
  \___4___/
*/

/* Secret Top */
module secret_top() {
    translate([0,0,11.1])
        linear_extrude(10)
            circle(d=unitX+eps,$fn=6);
    for(a=[0:1:5])
        rotate([0,0,a*60])
            translate([24.25,0,8.1])
                cylinder(d=5.1,h=20);
}
/*Dead end entering from side 1*/
module dead_end() {
    rotate([0,0,180]) {
        translate([0,-19,0])
            middle_hole();
        translate([0,-19,0])
            rotate([90,0,0])
                linear_extrude(unitX/2)
                    basic_path();
    }
}
/*Circular hole in the middle */
module middle_hole() {
    rotate_extrude(angle=360) {
        intersection() {
            basic_path();
            square(20);
        }
    }
}
/*Round path going from side 1 to side 3*/
module b_cut() {
    if(!Make_B_path_straight) {
    rotate([0,0,-60])
        translate([0,unitX,0])
            rotate_extrude(angle=360,$fn=96)
                translate([unitX*(sqrt(3)/2),0,0])
                    basic_path();
    } else {
        b_cut_straight();
    }
}
/*Straight path going from side 1 to side 3*/
module b_cut_straight() {
    rotate([0,0,120])
        hull() {
            rotate([0,0,240])
                translate([0,25])
                    middle_hole();
            rotate([0,0,120])
                translate([0,25])
                    middle_hole();
        }
}
/*Straight path going from side 1 to side 4*/
module a_cut() {
    rotate([90,0,0])
        linear_extrude(unitX,center=true)
            basic_path();
}
/*Straight path going from side 1 to the center*/
module a_cut_half() {
    rotate([90,0,180])
        linear_extrude(unitX/2)
            basic_path();
}
/*Path going from side 1 to sied 2*/
module c_cut() {
    rotate([0,0,60])
        translate([unitX/2,0,0])
            rotate_extrude(angle=360)
                translate([unitX/4,0,0])
                    basic_path();
}
/*Basic, featureless tile*/
module basic_tile() {
    difference() {
        union() {
            hull() {
                translate([0,0,15-eps])
                    linear_extrude(eps)
                        offset(r=-0.6)
                            doubleOffset(4)
                                circle(d=unitX,$fn=6);
                translate([0,0,5])
                    linear_extrude(9.4)
                        doubleOffset(4)
                            circle(d=unitX,$fn=6);
                translate([0,0,3])
                    linear_extrude(eps)
                        offset(r=-2)
                            doubleOffset(6)
                                circle(d=unitX,$fn=6);
                                
            }
            hull() {
                translate([0,0,0.6])
                    linear_extrude(2.6)
                        offset(r=-2)
                            doubleOffset(6)
                                circle(d=unitX,$fn=6);
                translate([0,0,0])
                linear_extrude(2.6)
                    offset(r=-2.6)
                        doubleOffset(6)
                            circle(d=unitX,$fn=6);
            }
        }
    }
}
/*Cutout on the bottom of the tile for keying orientation*/
module notch_cutout() {
    translate([0,-23,-eps])
        linear_extrude(3+eps,convexity=3) 
            difference() {
                square([12.7,10],center=true);
                translate([-0.4,-0.25,0])
                    square([0.8,5]);
            }
}

/*2d shape for cutting the ball paths*/
module basic_path() {
    translate([0,11.1,0]){
        hull() {
            circle(d=11);
            translate([0,2.2782,0])
                circle(d=11,$fn=4);
        }
    translate([0,4,0])
        square([8.75,5],center=true);
    }
}

/* A rule star */
module star(r) {
    scale([r,r,1])
    polygon([
        [0, 1], [-0.224514, 0.309017], 
        [-0.951057, 0.309017], [-0.363271, -0.118034], 
        [-0.587785, -0.809017], [0, -0.381966], 
        [0.587785, -0.809017], [0.363271, -0.118034], 
        [0.951057, 0.309017], [0.224514, 0.309017]
    ]);
}

/* Applies a fillet of radius R to a 2d module. 
   Pass a negative value for applying a fillet to
   a concave vertex. */
module doubleOffset(R=0) {
    offset(r=R)
        offset(r=-R)
            children();
}
