gutters=false;

module __CUSTOMIZER_LIMIT() {}
$fn=48;
hexSize=50;
unitY=hexSize;
unitX=hexSize*sqrt(3)*2/3;
eps=1/128;
minkowski() {
    intersection() {
        union() {
            rotate([0,0,180])
                path_vase();
            rotate([0,0,0])
                path_vase();
        }
        translate([0,0,19.7])
            cylinder(d=unitX-2,h=12,$fn=6);
    }
    cylinder(d=1.3,h=0.12,$fn=8);
}
difference() {
    union() {
        basic_tile();
    }
    rotate([0,0,180]) 
        notch_vase();
    rotate([0,0,0]) 
        notch_vase();
    path_vase();
    rotate([0,0,180])
        path_vase();
    if (gutters) {
    rotate([0,0,120])
        path_down();
    rotate([0,0,240])
        path_down();
    }
}

module path_down(bridge=true) {
    intersection() {
        translate([0,-25,0])
            rotate([-30,0,0])
                translate([0,15,-0.86]) {
                    a_cut();
                    cube([0.025,40,20],center=true);
                }
        hull() {
            translate([0,-6.35-0.65,0])
                cylinder(d=11,h=40,center=true);
            translate([0,-30,0])
                cylinder(d=11,h=40,center=true);
        }
    }
}

module path_vase() {
    intersection() {
    translate([0,-25,-1])
        rotate([30,0,0])
            translate([0,25,0])
               a_cut();
        translate([-25,-50+eps,0])
            cube(50);
    }
}

module notch_vase(slopeTop=true) {
    *if (slopeTop)
    translate([0,-22.8,0])
        rotate([60,0,0])
            translate([0,0,7.35])
                cube([12.7,10,20],center=true);
    translate([0,-23,0])
        cube([12.7,10,8],center=true);
}

/*Straight path going from side 1 to side 4*/
module a_cut() {
    rotate([90,0,0])
        linear_extrude(unitX,center=true)
            basic_path();
}

/*Basic, featureless tile*/
module basic_tile() {
    difference() {
        union() {
            hull() {
                /*translate([0,0,19.9-eps])
                    linear_extrude(eps)
                        offset(r=-3)
                            doubleOffset(6)
                                circle(d=unitX,$fn=6);
                */
                translate([0,0,19.7])
                    scale([0.8,1,1,])
                    linear_extrude(eps)
                        offset(r=-0.5)
                            doubleOffset(6)
                                circle(d=unitX,$fn=6);
                translate([0,0,5])
                    linear_extrude(9.4)
                        doubleOffset(4)
                            circle(d=unitX,$fn=6);
                translate([0,0,3])
                    linear_extrude(eps)
                        offset(r=-2)
                            doubleOffset(6)
                                circle(d=unitX,$fn=6);
            }
            hull() {
                translate([0,0,0.6])
                    linear_extrude(2.6)
                        offset(r=-2)
                            doubleOffset(6)
                                circle(d=unitX,$fn=6);
                translate([0,0,0])
                linear_extrude(2.6)
                    offset(r=-2.6)
                        doubleOffset(6)
                            circle(d=unitX,$fn=6);
            }
        }
    }
}
/*Cutout on the bottom of the tile for keying orientation*/
module notch_cutout() {
    translate([0,-23,-eps])
        linear_extrude(3+eps,convexity=3) 
            difference() {
                square([12.7,10],center=true);
                translate([-0.4,-0.25,0])
                    square([0.8,5]);
            }
}

/*2d shape for cutting the ball paths*/
module basic_path() {
    translate([0,11.1,0]){
        hull() {
            circle(d=11);
            translate([0,6.8,0])
                circle(d=eps,$fn=4);
        }
    }
}

/* Applies a fillet of radius R to a 2d module. 
   Pass a negative value for applying a fillet to
   a concave vertex. */
module doubleOffset(R=0) {
    offset(r=R)
        offset(r=-R)
            children();
}
