$fn=48;
hexSize=50;
unitY=hexSize;
unitX=hexSize*sqrt(3)*2/3;
eps=1/128;pattyHeight=11;
pattyDia=23.5;
inlay=false;

use <basic.scad>
if (inlay) 
render() intersection() {
    linear_extrude(12.8)
        circle(d=unitX,$fn=6);
    inlay_proto();
} else {

difference() {
    union() {
        hull() {
            translate([0,0,11.8])
                linear_extrude(1)
                    offset(r=-0.6)
                    doubleOffset(4)
                        circle(d=unitX,$fn=6);
            translate([0,0,6.1])
                linear_extrude(6.1)
                    doubleOffset(4)
                        circle(d=unitX,$fn=6);
        }
        for(a=[0:1:5])
            rotate([0,0,60*a])
                translate([24.348,0,1.6])
                    cylinder(d=4.75,h=9,$fn=56);
    }
    translate([0,0,6.1])
        patty();
    for(a=[0:1:2])
        rotate([0,0,60*a])
            translate([0,0,6.1])
                rotate([90,0,0])
                    cylinder(d=11,h=80,center=true);
    inlay_proto();
}
}

module inlay_proto() {
    translate([0,0,12])
        linear_extrude(3) {
            difference() {
                circle(d=40);
            rotate([0,0,150])
            question_mark();
            }
        }
}
module patty() {
rotate_extrude(angle=360,$fn=96)
    hull() {
        translate([pattyDia-pattyHeight/2,0])
            circle(d=pattyHeight);
        translate([pattyHeight/2,0])
            square(pattyHeight,center=true);
    }
}
module question_mark() {
    text("?", 
         size = 35,
         spacing=1,
         halign="center",
         valign="center",
         font = "Mandali:style=Regular",
         $fn = 16);
}