rows=4;
columns=5;
bottom=true;
reproduction=false;

module __CUSTOMIZER_LIMIT() {}
$fn=6; //Reappropriate circles as hexagons lol
hexSize=51;
unitHex=sqrt(3)*2/3;
unitY=hexSize;
unitX=unitHex*hexSize*1.5;
eps=1/128;
keyRad=reproduction?0:1.5;

/* How this works: 
1. Run a nested for loop to place slightly oversized hexagons
2. The hexagons get unioned into one contiguous polygon because
   they are overlapping
3. Apply offset(delta) to shrink the border to the right size
   while keeping a solid polygon which fits an array of 
   Hextraction slots
4. extrude this shape to the thickness of the Hextraction plate
5. Run the same nested for loops from step 1 to place 
   plate_cutout module, the negative space occupied inside a 
   single Hextraction slot
6. Take the difference between the results of steps 1-4 and 
   step 5.
*/
difference() {
    linear_extrude(7.75,convexity=3)
        offset(delta=-eps-0.1)
            hexgrid_placement() 
                circle(d=hexSize*unitHex+eps);
    hexgrid_placement() 
        plate_cutout();
}

/* Nested for loops for duplicating and placing child 
   modules relativeto the Hextraction grid size and the rows
   and columns parameters */
module hexgrid_placement() {
    for(b=[0:1:rows-1]) {
        for(a=[0:2:columns-1])  //Even parity hex grid part
            translate([unitX*a/2,unitY*b,0])
                children();
        for(a=[1:2:columns-1])  //Odd parity hex grid part
            translate([unitX*a/2,unitY*b+unitY/2,0])
                children();
    }
    if (bottom) { //make a parity flip if bottom is enabled
        for(a=[1:2:columns-1])
            translate([unitX/2*a,-unitY/2,0])
                children();
    }
        
}
/* Renders a single Hextraction plate slot */
module plate() {
    difference() {
        linear_extrude(7.75,convexity=3) // Rough Tile
            circle(d=51*unitHex);
        plate_cutout();
    }
}

/* Stuff needed to delete from a solid 51mm hexagon to 
   turn it into a Hextraction slot */
module plate_cutout() {
    translate([0,0,3])
        linear_extrude(7.75,convexity=3)
            difference() {
                circle(d=46.25*unitHex);
                offset(r=keyRad,$fn=12)
                    offset(delta=-keyRad)
                        translate([-11.7/2,-24.625])
                            square([11.7,6.125]);
            }
    translate([0,0,7.75]) //45 degree slope
        hull() {
            linear_extrude(1)
                circle(d=49.75*unitHex);
            translate([0,0,-1.75])
                linear_extrude(1)
                    circle(d=46.25*unitHex);
        }
    translate([0,0,-eps]) //hole through everything
        linear_extrude(3+eps+eps) {
            for (a=[1:1:5])
                rotate([0,0,180+a*60])
                    polygon([
                        [5.85,23.125],[5.85,20.5],
                        [7.85,18.5], [0,0], [-7.85,18.5],
                        [-5.85,20.5],[-5.85,23.125]
                    ]);
            circle(d=37*unitHex);
        }
}